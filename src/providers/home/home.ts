
 import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
//import { resolve } from 'url';
import { Http, Headers } from '@angular/http';

let apiUrl ="https://pragatitimes.com/pr_androidapp/news.php";
let apiSingleUrl ="https://pragatitimes.com/pr_androidapp/news_single.php";

@Injectable()
export class HomeProvider {

  constructor(public http: Http) {
    
  }
  getHeader()
  {
    let headers = new Headers();
    headers.append('Content-Type', 'text/html; charset=UTF-8');
    return headers;
  }


  getAllNews(val,page)
  {
    let url = apiUrl+'?id='+val+'&page='+page;
    return new Promise((resolve, reject) => {
      this.http.get(url,{})
        .subscribe(res => {
          resolve(res.json());
        }, (err) => {         
          reject(err);
        });
    });
  }

  getWeather_Info(){
    return this.http.get('https://api.weatherapi.com/v1/forecast.json?key=26247c1431e54067ba443302202006&q=Goa').map(res => res.json());
  }
  getSingleNews(id)
  {
    let url = apiSingleUrl+'?id='+id;
    return new Promise((resolve, reject) => {
      this.http.get(url,{})
        .subscribe(res => {
          resolve(res.json());
        }, (err) => {         
          reject(err);
        });
    });

  }
  getVideos()
  {
    let url = 'https://pragatitimes.com/pr_androidapp/video.php';
    return new Promise((resolve, reject) => {
      this.http.get(url,{})
        .subscribe(res => {
          resolve(res.json());
        }, (err) => {         
          reject(err);
        });
    });
  }
}
