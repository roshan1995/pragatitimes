import { Component } from '@angular/core';
import { HomeProvider } from '../../providers/home/home';
import {NavController, NavParams,LoadingController } from 'ionic-angular';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'page-detail-video',
  templateUrl: 'detail-video.html',
})
export class DetailVideoPage {
  newsList:any = [];
  id:number;
  constructor(public navCtrl: NavController, public navParams: NavParams,public home:HomeProvider,public sanitizer: DomSanitizer,public loadingController:LoadingController) {
    this.id = navParams.get('id');
    this.getNewsListing(this.id);
  }

  getNewsListing(id)
  {
    let loader = this.loadingController.create({
      content: "Loading News..."
    });  
    loader.present();
    this.home.getVideos().then((response:any)=>{
      if(response['count'] > 0)
      {
        
          this.newsList.push({
            title:response[id].title,
            image: response[id].image,
            content: this.sanitizer.bypassSecurityTrustResourceUrl(response[id].content),
            id: response[id].id,
          });
          loader.dismiss();
        }
      
    })
     
  }

}
