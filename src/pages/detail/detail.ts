import { Component } from '@angular/core';
import { HomeProvider } from '../../providers/home/home';
import { NavController, NavParams,LoadingController } from 'ionic-angular';
@Component({
  selector: 'page-detail',
  templateUrl: 'detail.html',
})
export class DetailPage {
  newsList:any = [];
  id:number;
  constructor(public navCtrl: NavController, public navParams: NavParams,public home:HomeProvider,public loadingController:LoadingController) {
    this.id = navParams.get('id');
    this.getNewsListing(this.id);
  }
  getNewsListing(id)
  {
    let loader = this.loadingController.create({
      content: "Loading News..."
    });  
    loader.present();
    this.home.getSingleNews(id).then((response:any)=>{
      if(response['count'] > 0)
      {
          this.newsList.push({
            title:response[0].title,
            image: response[0].image,
            content: response[0].content
          });
        loader.dismiss();
      }
    })
    
  }

}
