import { Component } from '@angular/core';
import { NavController,LoadingController } from 'ionic-angular';
import { HomeProvider } from '../../providers/home/home';
import { SocialSharing } from '@ionic-native/social-sharing';
import { DetailVideoPage } from '../detail-video/detail-video';
@Component({
  selector: 'page-video',
  templateUrl: 'video.html'
})
export class VideoPage {
  newsList:any = [];
  quotes :any;
  constructor(public navCtrl: NavController,public home:HomeProvider,private socialSharing: SocialSharing,public loadingController:LoadingController) {
    this.fetchTabData();
  }
  fetchTabData()
  {
    let loader = this.loadingController.create({
      content: "Loading News..."
    });  
    loader.present();
    this.home.getVideos().then((response:any)=>{
      if(response['count'] > 0)
      {
        for(let i = 0; i < response['count']; i++) 
        {
          this.newsList.push({
            title:response[i].title,
            image: response[i].image,
            content: response[i].content,
            share: response[i].share,
            id: i,
          });
        }
        loader.dismiss();
      }
    })
  }

  compilemsg(index):string{
    var msg = this.newsList[index].share + "-" + this.newsList[index].title ;
    return msg;
  }

  regularShare(index){
    var msg = this.compilemsg(index);
    this.socialSharing.share(msg, null, null, null);
  }
  twitterShare(index){
    var msg  = this.compilemsg(index);
    this.socialSharing.shareViaTwitter(msg, null, null);
  }
  whatsappShare(index){
    var msg  = this.compilemsg(index);
     this.socialSharing.shareViaWhatsApp(msg, null, null);
   }
   facebookShare(index){
     var msg  = this.compilemsg(index);
      this.socialSharing.shareViaFacebook(msg, null, null);
    }

    pushPage(id)
    {
      this.navCtrl.push(DetailVideoPage, {
        id: id
      });
    }
}
