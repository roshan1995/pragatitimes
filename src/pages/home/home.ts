import { Component } from '@angular/core';
import { HomeProvider } from '../../providers/home/home';
import { SocialSharing } from '@ionic-native/social-sharing';
import { Platform } from 'ionic-angular';
import { DetailPage } from '../detail/detail';
import { NavController,LoadingController  } from 'ionic-angular';
import { AdMobFree,AdMobFreeBannerConfig,AdMobFreeInterstitialConfig,AdMobFreeRewardVideoConfig } from '@ionic-native/admob-free';


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  selectedTab = 0;
  weatherIcon:string;
  weatherTemp:string;
  weatherText:string;
  time:string;
  newsList:any = [];
  slider:any = [];
  type_tab:any = 'home';
  items = [];
  count: number = 0;
  tabval: number = 0;
  page: number = 1;


  constructor(public home:HomeProvider,private socialSharing: SocialSharing,public navCtrl: NavController,private admobFree: AdMobFree,public loadingController:LoadingController,platform: Platform,) {
    this.fetchTabData('0');
    this.getWeatherInfo();
    platform.ready().then(() => {
    this.showBanner();
    this.showInterstitialAds();
    this.showRewardVideoAds();
    });
  }
  ngOnInit() {
  
  }
   showBanner(){
     let bannerConfig: AdMobFreeBannerConfig = {
       autoShow: true,
       id: "ca-app-pub-2999813196548113/5340632105",
       bannerAtTop:false
     };
     this.admobFree.banner.config(bannerConfig);
     this.admobFree.banner.prepare().then(() => {
     }).catch(e => alert(e));
   }

    showInterstitialAds(){
        let interstitialConfig: AdMobFreeInterstitialConfig = {
            autoShow: true,
            id: "ca-app-pub-3940256099942544/6300978111"
        };
        this.admobFree.interstitial.config(interstitialConfig);
        this.admobFree.interstitial.prepare().then(() => {
        }).catch(e => alert(e));
    }

    showRewardVideoAds(){
        let RewardVideoConfig: AdMobFreeRewardVideoConfig = {
            autoShow: true,
            id: "ca-app-pub-3940256099942544/6300978111"
        };
        this.admobFree.rewardVideo.config(RewardVideoConfig);
        this.admobFree.rewardVideo.prepare().then(() => {
        }).catch(e => alert(e));
    }

  fetchTabData(val)
  {
    if(this.tabval != val)
    {
      this.tabval=val;
      this.page=1;
      this.newsList=[];
    }
   let loader = this.loadingController.create({
      content: "Loading News..."
    });  
    loader.present();
    this.home.getAllNews(val,this.page).then((response:any)=>{
      if(response['count'] > 0)
      {
        for(let i = 0; i < response['count']; i++) 
        {
          this.newsList.push({
            title:response[i].title,
            image: response[i].image,
            content: response[i].content,
            link: response[i].link,
            id: response[i].id,
          });
        }
        for(let i = 0; i < response['count']; i++) 
        {
          this.slider.push({
            title:response[i].title,
            image: response[i].image,
            content: response[i].content,
            id: response[i].id,
          });
        }
        loader.dismiss();
      }
    })
  }

  doInfinite(infiniteScroll,val) {
    this.page = this.page+1;
    setTimeout(() => {
      this.home.getAllNews(val,this.page).then((response:any)=>{
        if(response['count'] > 0)
        {
          for(let i = 0; i < response['count']; i++) 
          {
            this.newsList.push({
              title:response[i].title,
              image: response[i].image,
              content: response[i].content,
              id: response[i].id,
            });
          }
        }
      })
  
      infiniteScroll.complete();
    }, 200);
  }


  getWeatherInfo()
  {
    this.home.getWeather_Info().subscribe((users) => {
     this.weatherIcon = "https:"+users['current']['condition']['icon'];
     this.weatherText = users['current']['condition']['text'];
     this.weatherTemp = users['current']['temp_c'];
     this.time = users['location']['localtime'];
    });
  }

  compilemsg(index):string{
    var msg = this.newsList[index].link + "-" + this.newsList[index].title ;
    return msg;
  }


  twitterShare(index){
    var msg  = this.compilemsg(index);
    this.socialSharing.shareViaTwitter(msg, null, null);
  }
  whatsappShare(index){
    var msg  = this.compilemsg(index);
     this.socialSharing.shareViaWhatsApp(msg, null, null);
   }
   facebookShare(index){
     var msg  = this.compilemsg(index);
      this.socialSharing.shareViaFacebook(msg, null, null);
    }

    pushPage(id)
    {
      this.navCtrl.push(DetailPage, {
        id: id
      });
    }

}
